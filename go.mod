module github.com/fasibio/portainer-api-cli

go 1.13

require (
	github.com/urfave/cli v1.22.2
	go.uber.org/zap v1.13.0
)
